<!DOCTYPE html>
<html lang="en">

	<!-- Head -->
	<head>
        <?php include('inc/head.inc.php') ?>
	</head>
	<!-- -->

	<body>
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
	
	        <!-- Video Section -->
            <?php include('inc/video.inc.php') ?>
	        <!--End Video Section-->
	        
	        <section class="main">
		        <div class="container">
			        
			        <h1><span>Основные средства</span></h1>

			        <div class="main__blue">
				        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>

				        <!-- 1 -->
				        <div class="content_box">
					        <div class="row">
						        <div class="col col_xs_6 col_gutter_lr align_center">
									<div class="align_center">
										<h4>База ЗАО «КГЭЦР»</h4>
										<p>Собственная территория площадью 1 га включает в себя транспортный участок, боксы для автотранспорта, токарный, электро-аккумуляторный, моторный и цех для сварки кессонов.</p>
									</div>
						        </div>
						        <div class="col col_xs_6 col_gutter_lr">
							        <div class="main_slider swiper-container">
								        <div class="swiper-wrapper">
									        <div class="swiper-slide">
										        <a data-fancybox="gallery" href="images/slide_01_01_lg.jpg">
											        <img src="images/slide_01_01_sm.jpg" class="img-fluid" alt="">
										        </a>
									        </div>
									        <div class="swiper-slide">
										        <a data-fancybox="gallery" href="images/slide_01_02_lg.jpg">
											        <img src="images/slide_01_02_sm.jpg" class="img-fluid" alt="">
										        </a>
									        </div>
									        <div class="swiper-slide">
										        <a data-fancybox="gallery" href="images/slide_01_03_lg.jpg">
											        <img src="images/slide_01_03_sm.jpg" class="img-fluid" alt="">
										        </a>
									        </div>
								        </div>
								        <!-- Add Arrows -->
								        <div class="swiper-button-prev"><i class="fas fa-angle-left"></i></div>
								        <div class="swiper-button-next"><i class="fas fa-angle-right"></i></div>
							        </div>
						        </div>
					        </div>
				        </div>
				        <!-- -->

				        <!-- 2 -->
				        <div class="content_box">
					        <div class="content_box_image block_left">
						        <div class="main_slider swiper-container">
							        <div class="swiper-wrapper">
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_1.jpg">
										        <img src="images/slide_02_01_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_2.jpg">
										        <img src="images/slide_02_02_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_3.jpg">
										        <img src="images/slide_02_03_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_4.jpg">
										        <img src="images/slide_02_04_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_5.jpg">
										        <img src="images/slide_02_05_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_6.jpg">
										        <img src="images/slide_02_06_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_7.jpg">
										        <img src="images/slide_02_07_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row2_8.jpg">
										        <img src="images/slide_02_08_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
							        </div>
							        <!-- Add Arrows -->
							        <div class="swiper-button-prev"><i class="fas fa-angle-left"></i></div>
							        <div class="swiper-button-next"><i class="fas fa-angle-right"></i></div>
						        </div>
					        </div>
					        <h4>Автопарк</h4>
					        <p>Наличие большого автопарка - одно из преимуществ нашей компании. В распоряжении специалистов есть все необходимое для достижения прекрасных результатов в работе.  В нашем автопарке имеются буровые установки различного назначения: ПБУ, УГБ-50М, УРБ-2А2, УГБ-ВС, ПБУ-2, АВБ-2М, УРБ-2Д3.</p>
					        <p>Это оборудование, отличающееся высокой производительностью и хорошей функциональностью. Оно предназначено для решения многих задач:</p>
					        <p>• разведка месторождений полезных ископаемых;</p>
					        <p>• проведение инженерно-геологических изысканий для строительства;</p>
					        <p>• бурение гидрогеологических скважин;</p>
					        <p>• бурение скважин хозяйственно-питьевого назначения;</p>
				        </div>
				        <!-- -->

				        <!-- 3 -->
				        <div class="content_box">

					        <div class="row">
						        <div class="col col_xs_6 col_gutter_lr align_center">
							        <div>
								        <h4>Ремонтно-механические мастерские</h4>
								        <p>Наши мастера регулярно проводят техническое обслуживание буровых установок, автомобилей в гараже и в автомастерской. Поэтому оборудование и техника находятся в хорошем состоянии. Они всегда готовы к работе.</p>
							        </div>
						        </div>
						        <div class="col col_xs_6 col_gutter_lr">
							        <div class="main_slider swiper-container">
								        <div class="swiper-wrapper">
									        <div class="swiper-slide">
										        <a data-fancybox="gallery" href="images/row3_1.jpg">
											        <img src="images/slide_03_01_sm.jpg" class="img-fluid" alt="">
										        </a>
									        </div>
									        <div class="swiper-slide">
										        <a data-fancybox="gallery" href="images/row3_2.jpg">
											        <img src="images/slide_03_02_sm.jpg" class="img-fluid" alt="">
										        </a>
									        </div>
									        <div class="swiper-slide">
										        <a data-fancybox="gallery" href="images/row3_3.jpg">
											        <img src="images/slide_03_03_sm.jpg" class="img-fluid" alt="">
										        </a>
									        </div>
								        </div>
								        <!-- Add Arrows -->
								        <div class="swiper-button-prev"><i class="fas fa-angle-left"></i></div>
								        <div class="swiper-button-next"><i class="fas fa-angle-right"></i></div>
							        </div>
						        </div>
					        </div>
				        </div>
				        <!-- -->

				        <!-- 4 -->
				        <div class="content_box">

					        <div class="content_box_image block_left">
						        <img src="images/slide_04_01_sm.jpg" class="img-fluid" alt="">
					        </div>
					        <h4>Геодезическое оборудование</h4>
					        <p>Это специальные приборы, предназначенные для решения широкого спектра геодезических работ. В распоряжении наших специалистов имеется современное оборудование, изготовленное на основании передовых технологий. Оно обеспечивает максимально точные результаты и существенно упрощает множество важных операций.</p>
					        <p>Наша компания уделяет огромное внимание материально-технической базе. Поэтому для выполнения геодезических работ используется наилучшее оборудование. К нему относится:</p>
					        <p>• тахеометр Leica TS02plus R500 (5") - устройство, предназначенное для измерения расстояний и углов. Оно оснащено программным обеспечением, необходимым для повседневного применения. К ключевым преимуществам тахеометра относится четкий просмотр  на дисплее с высоким разрешением, широкий диапазон безотражательных измерений, точность и надежность;</p>
					        <p>• GNSS приемник Trimble R5-RU - мощное геодезическое оборудование, идеально подходящее для работы в сложных условиях. С его помощью создается оптимальная система с каналом передачи данных. В приемнике используется инновационная технология и производительный процессор. С помощью такого оборудования легко выполняется необходимое количество измерений в любых условиях.</p>
					        <p>производительный процессор. С помощью такого оборудования легко выполняется необходимое количество измерений в любых условиях.
						        Дополнительно в распоряжении специалистов имеются лазерные рулетки, отражатели, портативные средства связи. Геодезическое оборудование в обязательном порядке проходит поверку в метрологических лабораториях. </p>
				        </div>
				        <!--  -->

				        <!-- 5 -->
				        <div class="content_box">

					        <div class="content_box_image block_right">
						        <div class="main_slider swiper-container">
							        <div class="swiper-wrapper">
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_1.jpg">
										        <img src="images/slide_05_01_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_2.jpg">
										        <img src="images/slide_05_02_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_3.jpg">
										        <img src="images/slide_05_03_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_4.jpg">
										        <img src="images/slide_05_04_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_5.jpg">
										        <img src="images/slide_05_05_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_6.jpg">
										        <img src="images/slide_05_06_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_7.jpg">
										        <img src="images/slide_05_07_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="swiper-slide">
									        <a data-fancybox="gallery" href="images/row5_8.jpg">
										        <img src="images/slide_05_08_sm.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
							        </div>
							        <!-- Add Arrows -->
							        <div class="swiper-button-prev"><i class="fas fa-angle-left"></i></div>
							        <div class="swiper-button-next"><i class="fas fa-angle-right"></i></div>
						        </div>
					        </div>

					        <h4>Лабораторный корпус</h4>
					        <p>Для качественного решения поставленных задач у специалистов есть все необходимое. Имеется в виду оборудование, с помощью которого выполняется химический анализ воды, исследуются физические свойства глинистого сырья, определяется состав силикатных пород. Также сотрудники лаборатории определяют физические свойства щебня, гравия, песчаных грунтов, известняка.</p>
					        <p>Комплексные исследования выполняются в лаборатории, занимающей большую площадь. Лаборатория оснащена оборудованием, предназначенным для проведения анализов. Также в распоряжении специалистов имеются расходные материалы в достаточном количестве.</p>
					        <p>главным устройствам, используемым в лаборатории, относятся:</p>
					        <p>• камнерезные станки - оборудование, предназначенное для резки природного камня. Оно обладает высокими эксплуатационными показателями;</p>
					        <p>• прессы П-125, П-50 - лабораторные прессы, с помощью которых проводятся испытания образцов на сжатие. Они оснащены гидравлическим силоизмерителем. В оборудовании предусмотрено автоматическое ограничение нагрузки;</p>
					        <p>• анализаторы жидкости - устройства, предназначенные для определения концентрации специфических соединений в почве, воде. Они отличаются большой емкостью встроенной памяти, эргономичностью и прекрасной функциональностью. В анализаторах жидкости внедрены усовершенствованные алгоритмы обработки результатов.</p>
					        Это далеко не все лабораторное оборудование. У нас имеются пусковые и валковые дробилки, электропечи, лабораторные весы, истиратели, тонометры и радиометры. Также лаборатория оснащена измерительными приборами разных видов и модификаций.</p>
					        <p>Все оборудование, которое нуждается в периодической поверке, поверяется в соответствии с установленными требованиями. Такой факт подтверждается свидетельствами. Также у нас имеется разрешение на проведение широкого круга лабораторных исследований.</p>
				        </div>
				        <!-- -->


				        <!-- 6 -->
				        <div class="content_box">
					        <div class="row">
						        <div class="col col_xs_6 col_gutter_lr">
							        <img src="images/slide_06_01_sm.jpg" class="img-fluid" alt="">
						        </div>
						        <div class="col col_xs_6 col_gutter_lr align_center">
							        <div>
								        <h4>Кернохранилище</h4>
								        <p>Керн - образец горной породы, извлеченный из скважины.</p>
								        <p>Это своеобразный документ, с помощью которого изучается строение месторождения полезного ископаемого. Керн направляется в специальное хранилище. Наши геологи описывают образец и берут из него пробы для дальнейшего проведения лабораторных исследований.</p>
								        <br/>
								        <br/>
							        </div>
						        </div>
					        </div>
				        </div>
				        <!-- -->

			        </div>
		        </div>
	        </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->
	
  </body>
</html>