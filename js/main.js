$(document).ready(function() {


    $(".btn_modal").fancybox({
        'padding'    : 0
    });


    var swiper = new Swiper('.s1', {
        slidesPerView: 5,
        spaceBetween: 10,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.swiper-button-next_1',
            prevEl: '.swiper-button-prev_1',
        },
    });

    var swiper1 = new Swiper('.s2', {
        slidesPerView: 5,
        spaceBetween: 25,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    $('.dropdown').mouseover(function(){
        $('.dropdown-menu').show();
    });
    $('.dropdown').mouseleave(function(){
        $('.dropdown-menu').hide();
    });


    var mainGallery = new Swiper('.main_slider', {
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

});
