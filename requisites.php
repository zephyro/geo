<!DOCTYPE html>
<html lang="en">

	<!-- Head -->
	<head>
	    <?php include('inc/head.inc.php') ?>
	</head>
	<!-- -->

	<body>
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	
	        <!-- Video Section -->
            <?php include('inc/video.inc.php') ?>
	        <!--End Video Section-->
	        
	        <section class="main main_bg_line">
		        <div class="container">
			        
			        <h1><span>Реквизиты</span></h1>
			        
			        <div class="requisites">
				        
				        <div class="requisites_item">
					        <div class="requisites_item_wrap">
						        <div class="requisites_logo">
							        <img src="img/main_logo.svg" class="img-fluid" alt="">
						        </div>
					        </div>
				        </div>
				
				        <div class="requisites_item">
					        <div class="requisites_item_wrap">
						        <ul>
							        <li>Организационно-правовая форма: <br/>Закрытое акционерное общество</li>
							        <li>Форма собственности <br/>Частная</li>
							        <li>Полное наименование организации <br/>Закрытое акционерное общество «Комплексная геологическая экспедиция Центральных районов»</li>
							        <li>Сокращенное наименование организации ЗАО «КГЭЦР»</li>
							        <li>Дата регистрации <br/>15.06.1998 г.</li>
						        </ul>
					        </div>
				        </div>
				
				        <div class="requisites_item">
					        <div class="requisites_item_wrap">
						        <ul>
							        <li>ОГРН <br/>1023300997565</li>
							        <li>ИНН <br/>3316005730</li>
							        <li>КПП <br/>331601001</li>
							        <li>ОКПО <br/>002 899 40</li>
							        <li>ОКВЭД <br/>71.12.3</li>
							        <li>ОКТМО <br/>17630101</li>
						        </ul>
					        </div>
				        </div>
				
				        <div class="requisites_item">
					        <div class="requisites_item_wrap">
						        <ul>
							        <li>Юридический и фактический адрес <br/>601010,  Владимирская обл, г. Киржач, <br/>ул. Юбилейная, д. 20</li>
							        <li>Телефон <br/>8 (49237) 2 - 16 - 03, 8 (919) 023 - 89 - 61</li>
							        <li>сайт <br/>Геология-33.рф</li>
							        <li>электронная почта  <br/>kirzhach_geolog@mail.ru</li>
						        </ul>
					        </div>
				        </div>
				
				        <div class="requisites_item">
					        <div class="requisites_item_wrap">
						        <ul>
							        <li>Расчетный счет <br/>407 028 100 96 21  00000 12</li>
							        <li>Банк <br/>ПАО Росбанк, г. Москва</li>
							        <li>БИК <br/>0445 25 256</li>
							        <li>Кор. сч.<br/>301 018 100 000 000 00  256</li>
						        </ul>
					        </div>
				        </div>
				
				        <div class="requisites_item">
					        <div class="requisites_item_wrap">
						        <ul>
							        <li>Ф.И.О. директора <br/>(наименование должности) <br/><strong>Четвериков Алексей Викторович</strong> <br/>Директор</li>
							        <li>Действующего на основании <br/>Устава</li>
							        <li>Ф.И.О. главного бухгалтера (телефон) <br/>Яковенко Алла Ивановна <br/>8 ( 49237) 2 - 29 - 00</li>
						        </ul>
					        </div>
				        </div>
			
			        </div>
			        
			        <div class="text_center">
				        <a href="#" class="btn_main">Скачать реквизиты</a>
			        </div>
			        
		        </div>
	        </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
        
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->
	
  </body>
</html>