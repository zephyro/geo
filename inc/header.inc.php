<header>
    <div class="container">
        <div class="row">
            <div class="col-left">
                <div class="block-center">
                    <div class="phone-icon"><img src="img/phone-blue.png" alt="">&nbsp;&nbsp;<a href="tel:+74923721603" class="tel">+7 (49237) 2-16-03</a></div>
                    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="tel:88001248798" class="tel">8-800-124-87-98</a></div>
                    <div><img src="img/envelope-blue.png" alt="">&nbsp; <a href="mailto:kirzhach_geolog@mail.ru"  class="tel">kirzhach_geolog@mail.ru</a></div>
                </div>
            </div>
            <div class="col-right">
                <div class="brand"></div>
                <ul class="navbar-nav navbar-menu">
                    <li><a href="#">О компании <i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Геология <i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Гидрогеология <i class="fas fa-chevron-down"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Геодезия <i class="fas fa-chevron-down"></i></a>
                        <ul class="dropdown-menu mdrop">
                            <div><a href="#">Инженерно-геодезические изыскания</a></div>
                            <div><a href="#">Маркшейдерское обслуживание горных работ</a></div>
                        </ul>
                    </li>
                    <li><a href="#">Водоснабжение <i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Контакты <i class="fas fa-chevron-down"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</header>