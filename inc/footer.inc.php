<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-2">
                <div class="logo-footer"></div>
            </div>
            <div class="col-xs-7">
                <nav>
                    <ul class="footer-navbar">
                        <li><a href="#">О компании </a></li>
                        <li><a href="#">Геология </a></li>
                        <li><a href="#">Гидрогеология </a></li>
                        <li><a href="#">Геодезия</a></li>
                        <li><a href="#">Водоснабжение </a></li>
                        <li><a href="#">Контакты </a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-xs-3 col-footer">
                <div class="phone-icon"><img src="img/phone.png" alt=""> <a href="tel:+7 (49237) 2-16-03" class="footer_tel">+7 (49237) 2-16-03</a></div>
                <div><img src="img/envelope.png" alt=""> <a href="mailto:kirzhach_geolog@mail.ru" class="footer_tel">kirzhach_geolog@mail.ru</a></div>
                <div><img src="img/map-market.png" alt=""> г.Киржач, ул. Юбилейная, д.20</div>
            </div>
        </div>
    </div>
</footer>