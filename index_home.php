<!DOCTYPE html>
<html lang="en">

	<!-- Head -->
	<head>
	    <?php include('inc/head.inc.php') ?>
	</head>
	<!-- -->

	<body>
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <!-- Video Section -->
            <?php include('inc/video.inc.php') ?>
		    <!--End Video Section-->
	
	        <section class="shape">
	      <div class="container">
	        <div class="row">
	          <h1 class="works">Виды деятельности</h1>
	          <div class="col-lg-2">
	            <div class="work-img">
	              <img src="img/1.png" alt="">
	            </div>
	            <p>Геологические изыскания</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="work-img">
	              <img src="img/2.png" alt="">
	            </div>
	            <p>Гидрогеологические изыскания</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="work-img">
	              <img src="img/3.png" alt="">
	            </div>
	            <p>Геологические и маркшейдерское обслуживание карьеров</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="work-img">
	              <img src="img/4.png" alt="">
	            </div>
	            <p>Инженерно-геологические, инженерно-геодезические работы под строительство</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="work-img">
	              <img src="img/5.png" alt="">
	            </div>
	            <p>Бурение скважин на воду</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="work-img">
	              <img src="img/6.png" alt="">
	            </div>
	            <p>Автономное водоснабжение, канализация, водоподготовка</p>
	          </div>
	        </div>
	      </div>
	    </section>
	
	        <section class="palette">
	      <div class="container">
	        <div class="row">
	          <h1 class="license">Лицензии</h1>
	          <div class="block-center group" style="background-image: url(img/group.png)"></div>
	        </div>
	      </div>
	    </section>
	
	        <section class="figures">
	      <div class="container">
	        <div class="row">
	          <div class="col-lg-3">
	            <h3>1979</h3>
	            <p>год основания</p>
	          </div>
	          <div class="col-lg-3">
	            <h3>700</h3>
	            <p>разведанных месторождений полезных ископаемых</p>
	          </div>
	          <div class="col-lg-3">
	            <h3>1 000</h3>
	            <p>геологических отчетов, заключений по инженерно-геологическим, инженерно-экологическим изысканиям</p>
	          </div>
	          <div class="col-lg-3">
	            <h3>15 000</h3>
	            <p>пробуренных скважин хозяйственно-питьевого назначения</p>
	          </div>
	        </div>
	      </div>
	    </section>
	  
	        <section class="types">
	      <div class="container">
	        <div class="row">
	          <h1 class="type-text">Типовые услуги</h1>
	          <div class="col-lg-2">
	            <div class="type-img" style="background-image: url(img/t1.png)"></div>
	            <p>Поисково-оценочное бурение</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="type-img" style="background-image: url(img/t2.png)"></div>
	            <p>Бурение фильтровых скважин на воду</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="type-img" style="background-image: url(img/t3.png)"></div>
	            <p>Бурение артезианских скважин</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="type-img" style="background-image: url(img/t4.png)"></div>
	            <p>Химический анализ воды</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="type-img" style="background-image: url(img/t5.png)"></div>
	            <p>Системы очистки воды</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="type-img" style="background-image: url(img/t6.png)"></div>
	            <p>Монтаж водоснабжения с кессоном</p>
	          </div>
	        </div>
	      </div>
	    </section>
	
	        <section class="advantages">
	      <div class="container">
	        <div class="row">
	          <h1 class="advantage-text">Наши преимущества</h1>
	          <div class="col-lg-1"></div>
	          <div class="col-lg-2">
	            <div class="advantage-img" style="background-image: url(img/a1.png)"></div>
	            <p>Собственный парк техники, ремонтная база, лаборатория</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="advantage-img" style="background-image: url(img/a2.png)"></div>
	            <p>Квалифицированные специалисты</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="advantage-img" style="background-image: url(img/a3.png)"></div>
	            <p>Соблюдение сроков</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="advantage-img" style="background-image: url(img/a4.png)"></div>
	            <p>Гарантия качества</p>
	          </div>
	          <div class="col-lg-2">
	            <div class="advantage-img" style="background-image: url(img/a5.png)"></div>
	            <p>Работа по договору</p>
	          </div>
	          <div class="col-lg-1"></div>
	        </div>
	      </div>
	    </section>
	
	        <section class="objects">
	      <h1 class="foto-text">Фотографии с объектов</h1>
	      <!-- Swiper -->
	      <div class="swiper-container s1">
	        <div class="swiper-wrapper">
	          <div class="swiper-slide" style="background-image: url(img/slide-3.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-2.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-3.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-4.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-5.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-3.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-2.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-3.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-4.png)"></div>
	          <div class="swiper-slide" style="background-image: url(img/slide-5.png)"></div>
	        </div>
	        <!-- Add Pagination -->
	        <!-- <div class="swiper-pagination"></div> -->
	        <!-- Add Arrows -->
	       <!--  <div class="swiper-button-next"></div>
	        <div class="swiper-button-prev"></div> -->
	      </div>
	    </section>
	  
	
	        <section class="partner-slider">
	      <div class="container">
	        <div class="row">
	          <h1 class="our-clients">Наши клиенты</h1>
	        </div>
	          <div class='col-md-12'>
	            <div class="swiper-container s2">
	              <div class="swiper-wrapper partners-slider">
	                <div class="swiper-slide" >
	                  <div style="background-image: url(img/sl1.png)"></div>
	                </div>
	                <div class="swiper-slide">
	                  <div style="background-image: url(img/sl2.png)"></div>
	                </div>
	                <div class="swiper-slide">
	                  <div style="background-image: url(img/sl3.png)"></div>
	                </div>
	                <div class="swiper-slide">
	                  <div style="background-image: url(img/sl4.png)"></div>
	                </div>
	                <div class="swiper-slide">
	                  <div style="background-image: url(img/sl5.png)"></div>
	                </div>
	                <div class="swiper-slide">
	                  <div style="background-image: url(img/sl6.png)"></div>
	                </div>
	              </div>
	        <!-- Add Pagination -->
	        <!-- <div class="swiper-pagination"></div> -->
	        <!-- Add Arrows -->
	           
	           </div>
	           <div class="swiper-button-next"></div>
	            <div class="swiper-button-prev"></div>
	          </div>
	        </div>
	    </section>
	
	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
        
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->
	
  </body>
</html>